#!/bin/bash

set -euo pipefail



function python() {
    if [[ -x ./venv/bin/python ]] ; then
        ./venv/bin/python "$@"
    elif [[ -x /venv/bin/python ]] ; then
        /venv/bin/python "$@"
    else
        python3 "$@"
    fi
}

function get_profile_values() {
    local instantiation
    if [[ -z ${1:-} ]] ; then
        instantiation="argon2.PasswordHasher()"
    else
        instantiation="argon2.PasswordHasher.from_parameters( argon2.profiles.$1 )"
    fi

    python -c "
import argon2
a = ${instantiation}
print( a.time_cost, a.memory_cost, a.parallelism, a.hash_len )
"
}

function expand_range() {
    local lhs="${1%..*}"
    local rhs="${1#*..}"
    for v in $( seq $lhs $rhs ) ; do
        echo $v
    done
}

function expand_list() {
    local values="$( echo $1 | tr ',' ' ' )"
    for v in $values ; do
        echo $v
    done
}

function expand_values() {
    if [[ ${1#*..} != $1 ]] ; then
        expand_range "$1"
    else
        expand_list "$1"
    fi
}

function hash_time() {
    local output="$( python -m argon2 -t $1 -m $2 -p $3 -l $4  )"
    #echo "$output" 1>&2
    echo "$output" | awk 'END { print $1 }'
}



CUSTOM=false
DEFAULT_VALUES="$( get_profile_values )"
DEFAULT_TIME_COST="$( echo "$DEFAULT_VALUES" | awk '{ print $1 }' )"
DEFAULT_MEMORY_COST="$( echo "$DEFAULT_VALUES" | awk '{ print $2 }' )"
DEFAULT_PARALLELISM="$( echo "$DEFAULT_VALUES" | awk '{ print $3 }' )"
DEFAULT_HASH_LENGTH="$( echo "$DEFAULT_VALUES" | awk '{ print $4 }' )"

if [[ -n ${TIME_COSTS:-} ]] ; then
    TIME_COSTS="$( expand_values "$TIME_COSTS" )"
    CUSTOM=true
else
    TIME_COSTS="$DEFAULT_TIME_COST"
fi

if [[ -n ${MEMORY_COSTS:-} ]] ; then
    MEMORY_COSTS="$( expand_list "$MEMORY_COSTS" )"
    CUSTOM=true
else
    MEMORY_COSTS="$DEFAULT_MEMORY_COST"
fi

if [[ -n ${PARALLELISMS:-} ]] ; then
    PARALLELISMS="$( expand_values "$PARALLELISMS" )"
    CUSTOM=true
else
    PARALLELISMS="$DEFAULT_PARALLELISM"
fi

if [[ -n ${HASH_LENGTHS:-} ]] ; then
    HASH_LENGTHS="$( expand_list "$HASH_LENGTHS" )"
    CUSTOM=true
else
    HASH_LENGTHS="$DEFAULT_HASH_LENGTH"
fi


printf "%s,%s,%s,%s,%s,%s\n" profile time_cost memory_cost parallelism hash_length verification_time
time="$( hash_time $DEFAULT_TIME_COST $DEFAULT_MEMORY_COST $DEFAULT_PARALLELISM $DEFAULT_HASH_LENGTH )"
printf "%s,%s,%s,%s,%s,%s\n" default $DEFAULT_TIME_COST $DEFAULT_MEMORY_COST $DEFAULT_PARALLELISM $DEFAULT_HASH_LENGTH $time

if [[ -n ${PROFILES:-} ]] ; then
    for profile in $( expand_list "$PROFILES" ) ; do
        values="$( get_profile_values "$profile" )"
        time="$( hash_time $values )"
        printf "%s,%s,%s,%s,%s,%s\n" $profile $values $time
    done
fi

if [[ $CUSTOM == true ]] ; then
    for TIME_COST in $TIME_COSTS ; do
        for MEMORY_COST in $MEMORY_COSTS ; do
            for PARALLELISM in $PARALLELISMS ; do
                for HASH_LENGTH in $HASH_LENGTHS ; do
                    time="$( hash_time $TIME_COST $MEMORY_COST $PARALLELISM $HASH_LENGTH )"
                    printf "%s,%s,%s,%s,%s,%s\n" custom $TIME_COST $MEMORY_COST $PARALLELISM $HASH_LENGTH $time
                done
            done
        done
    done
fi

