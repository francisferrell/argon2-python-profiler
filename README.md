
# Argon2 Python Benchmark

A docker image which benchmarks python's `argon2-cffi`.

Using this docker image, you can characterize argon2's performance in a specific docker environment
with its default configuration, named profiles, and with specific parameters.

Under the hood, this application uses the argon2 CLI and outputs its findings in CSV format.
Read about the CLI here: https://argon2-cffi.readthedocs.io/en/stable/cli.html

This docker image is rebuilt monthly with the latest python3 and the latest `argon2-cffi`.

# Examples

No arguments:

```sh
docker run --rm -it francisferrell/argon2-python-benchmark
profile,time_cost,memory_cost,parallelism,hash_length,verification_time
default,3,65536,4,32,45.5ms
```

Some profiles:

```sh
docker run --rm -it -e PROFILES='PRE_21_2 CHEAPEST' francisferrell/argon2-python-benchmark
profile,time_cost,memory_cost,parallelism,hash_length,verification_time
default,3,65536,4,32,45.6ms
PRE_21_2,2,102400,8,16,66.7ms
CHEAPEST,1,8,1,4,0.0ms
```

Some parameters:

```sh
docker run --rm -it -e TIME_COSTS='1,2' francisferrell/argon2-python-benchmark
profile,time_cost,memory_cost,parallelism,hash_length,verification_time
default,3,65536,4,32,45.2ms
custom,1,65536,4,32,22.6ms
custom,2,65536,4,32,34.1ms
```

All together:

```sh
docker run --rm -it -e PROFILES='PRE_21_2 CHEAPEST' -e TIME_COSTS='1,2' francisferrell/argon2-python-benchmark
profile,time_cost,memory_cost,parallelism,hash_length,verification_time
default,3,65536,4,32,44.9ms
custom,1,65536,4,32,22.9ms
custom,2,65536,4,32,34.4ms
PRE_21_2,2,102400,8,16,65.7ms
CHEAPEST,1,8,1,4,0.0ms
```


# Selecting Profiles and Parameters

The application will always benchmark the default argon2 configuration. You can instruct the
application to benchmark other configurations by providing environment variables.

## Profiles

Set the `PROFILES` environment variable to the names of the argon2 package's profiles you wish to
benchmark, without the `argon2.profiles.` prefix. This env var may be a space separated or a comma
separated list of profile names.

Read about profiles here: https://argon2-cffi.readthedocs.io/en/stable/api.html#module-argon2.profiles.

For example:

```sh
PROFILES='PRE_21_2 CHEAPEST'
```

```sh
PROFILES='PRE_21_2,CHEAPEST'
```


## Parameters

Set any of the environment variables in the next 4 sections to benchmark a combination of custom
configurations.

Any paremter fields that are omitted will take their value from the default profile. That is, if
you run the application with `TIME_COSTS` and `MEMORY_COSTS` specified, then each time cost and
memory cost value is benchmarked in combination with the parallelism and hash length of the default
profile.

Read about choosing parameters here: https://argon2-cffi.readthedocs.io/en/stable/parameters.html.

When specifying multiple parameters, all possible combinations are benchmarked. For example, if you
run the application with `TIME_COSTS='1..3'` and `MEMORY_COSTS='16384 32768'` then all six
permutations will be benchmarked:

```sh
-t 1 -m 16384
-t 2 -m 16384
-t 3 -m 16384
-t 1 -m 32768
-t 2 -m 32768
-t 3 -m 32768
```

### Time Cost

The `TIME_COSTS` environment variable accepts a space separated list, a comma separated list, or a
range of values. All of the following are equivalent:

```sh
TIME_COSTS='1 2 3'
TIME_COSTS='1,2,3'
TIME_COSTS='1..3'
```

### Memory Cost

The `MEMORY_COSTS` environment variable accepts a space separated list or a comma separated list.
All of the following are equivalent:

```sh
MEMORY_COSTS='65536 32768 16384'
MEMORY_COSTS='65536,32768,16384'
```

### Parallelism

The `PARALLELISMS` environment variable accepts a space separated list, a comma separated list, or
a range of values. All of the following are equivalent:

```sh
PARALLELISMS='2 3 4'
PARALLELISMS='2,3,4'
PARALLELISMS='2..4'
```

### Hash Length

The `HASH_LENGTHS` environment variable accepts a space separated list or a comma separated list.
All of the following are equivalent:

```sh
HASH_LENGTHS='16 32'
HASH_LENGTHS='16,32'
```


# Links

- Source: https://gitlab.com/francisferrell/argon2-python-benchmark
- Docker Hub: https://hub.docker.com/r/francisferrell/argon2-python-benchmark

